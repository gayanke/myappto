import Vue from 'vue'
import App from './App.vue'

import * as VueGoogleMaps from "vue2-google-maps" // Import package


Vue.component('add-google-map', require('./components/AddGoogleMap.vue').default);

Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDNQGaGlfCgQGNsK1uYWJQw3cHeQlZZHU8",
    libraries: "places"
  }
});

new Vue({
  render: h => h(App),
}).$mount('#app')